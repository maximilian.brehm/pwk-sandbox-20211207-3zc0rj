# Entrypoint is also needed as image by default set `terragrunt` binary as an
# entrypoint.
image:
  name: cytopia/terragrunt:0.12-0.23
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# GENERAL
variables:
  PIPELINE: default
  PLAN: plan.tfplan
  JSON_PLAN_FILE: tfplan.json
  GOOGLE_APPLICATION_CREDENTIALS: ${CI_PROJECT_DIR}/serviceaccount.json
  #VAR_FILE_PATH: ${CI_PROJECT_DIR}/cloud_run/secrets.auto.tfvars

default:
  tags: ["docker"]


before_script:
  - echo -e "machine gitlab.com\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > ~/.netrc
#   We wanna use git::ssh locally, but git::https in pipeline
  - find . -name "*.tf" -exec cat {} + | grep "git:"
  #- find . -name "*.tf" -exec sed  -i "s/git@gitlab.com:/https:\/\/gitlab.com\//g" {} +
  - find . -name "*.tf" -exec sed  -i "s/git@gitlab.com:/https:\/\/gitlab.com\//g" {} +
  #- sed 's#git@gitlab.com:#https://gitlab.com/#g' -i */*.tf
  - find . -name "*.tf" -exec cat {} + | grep "git:"
  #- sed 's#git@gitlab.com:#https://gitlab.com/#g' -i */*.tf
  #- sed 's#git@gitlab.com:#https://gitlab.com/#g' -i */*/*.tf
  #- sed 's#git@gitlab.com:#https://gitlab.com/#g' -i */*/*/*.tf
# Get secret json key to manage project
#  - echo $TF_SERVICE_ACCOUNT_JSON | base64 -d > ${GOOGLE_APPLICATION_CREDENTIALS}
# Get secrets for application
#  - echo "$SECRETS_AUTO_TFVARS" > ${VAR_FILE_PATH}
  - cat 
# Simpe alias to get pretty gitlab formatting
  - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
  - apk --no-cache add curl jq
# If No credentials - exit
  #- git clone  https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/peakwork/it/lad/terraform-infra/modules/module-service-account-gce.git


# EXTENDS
.plan_part_production:
  script:
    - echo $PROD_TF_SERVICE_ACCOUNT_JSON | base64 -d > ${GOOGLE_APPLICATION_CREDENTIALS}
    - if [[ ! -s ${GOOGLE_APPLICATION_CREDENTIALS} ]]; then echo "No google credentials found"; exit 1; fi
    - cd ${DIR_NAME}
    - cp ../Production/${DIR_NAME}.auto.tfvars .
    - cp ../Production/terragrunt.hcl ../
    - terragrunt init
    - terragrunt plan -out=${PLAN}
    - terraform show --json ${PLAN} | convert_report > ${JSON_PLAN_FILE}
  artifacts:
    paths:
      - ${DIR_NAME}/${PLAN}
    reports:
      terraform: ${DIR_NAME}/${JSON_PLAN_FILE}
  only:
    refs:
      - production
    variables:
      - ($PIPELINE ==  "default" || $PIPELINE == $DIR_NAME)

.plan_part_stage:
  script:
    - echo $STAGE_TF_SERVICE_ACCOUNT_JSON | base64 -d > ${GOOGLE_APPLICATION_CREDENTIALS}
    - if [[ ! -s ${GOOGLE_APPLICATION_CREDENTIALS} ]]; then echo "No google credentials found"; exit 1; fi
    - cd ${DIR_NAME}
    - cp ../Stage/${DIR_NAME}.auto.tfvars .
    - cp ../Stage/terragrunt.hcl ../
    - terragrunt init
    - terragrunt plan -out=${PLAN}
    - terraform show --json ${PLAN} | convert_report > ${JSON_PLAN_FILE}
  artifacts:
    paths:
      - ${DIR_NAME}/${PLAN}
    reports:
      terraform: ${DIR_NAME}/${JSON_PLAN_FILE}
  only:
    refs:
      - stage
    variables:
      - ($PIPELINE ==  "default" || $PIPELINE == $DIR_NAME)

.plan_part_dev:
  script:
    - echo $DEV_TF_SERVICE_ACCOUNT_JSON | base64 -d > ${GOOGLE_APPLICATION_CREDENTIALS}
    - if [[ ! -s ${GOOGLE_APPLICATION_CREDENTIALS} ]]; then echo "No google credentials found"; exit 1; fi
    - cd ${DIR_NAME}
    - cp ../Dev/${DIR_NAME}.auto.tfvars .
    - cp ../Dev/terragrunt.hcl ../
    - terragrunt init
    - terragrunt plan -out=${PLAN}
    - terraform show --json ${PLAN} | convert_report > ${JSON_PLAN_FILE}
  artifacts:
    paths:
      - ${DIR_NAME}/${PLAN}
    reports:
      terraform: ${DIR_NAME}/${JSON_PLAN_FILE}
  only:
    refs:
      - dev
    variables:
      - ($PIPELINE ==  "default" || $PIPELINE == $DIR_NAME)


.apply_part_production:
  script:
     - echo $PROD_TF_SERVICE_ACCOUNT_JSON | base64 -d > ${GOOGLE_APPLICATION_CREDENTIALS}
     - if [[ ! -s ${GOOGLE_APPLICATION_CREDENTIALS} ]]; then echo "No google credentials found"; exit 1; fi
     - cd ${DIR_NAME}
     - cp ../Production/${DIR_NAME}.auto.tfvars .
     - cp ../Production/terragrunt.hcl ../
     - terragrunt apply -auto-approve
  when: manual
  only: 
    refs:
      - production
    variables:
      - ($PIPELINE ==  "default" || $PIPELINE == $DIR_NAME)

.apply_part_stage:
  script:
     - echo $STAGE_TF_SERVICE_ACCOUNT_JSON | base64 -d > ${GOOGLE_APPLICATION_CREDENTIALS}
     - if [[ ! -s ${GOOGLE_APPLICATION_CREDENTIALS} ]]; then echo "No google credentials found"; exit 1; fi
     - cd ${DIR_NAME}
     - cp ../Stage/${DIR_NAME}.auto.tfvars .
     - cp ../Stage/terragrunt.hcl ../
     - terragrunt apply -auto-approve
  when: manual
  only: 
    refs:
      - stage
    variables:
      - ($PIPELINE ==  "default" || $PIPELINE == $DIR_NAME)

.apply_part_dev:
  script:
     - echo $DEV_TF_SERVICE_ACCOUNT_JSON | base64 -d > ${GOOGLE_APPLICATION_CREDENTIALS}
     - if [[ ! -s ${GOOGLE_APPLICATION_CREDENTIALS} ]]; then echo "No google credentials found"; exit 1; fi
     - cd ${DIR_NAME}
     - cp ../Dev/${DIR_NAME}.auto.tfvars .
     - cp ../Dev/terragrunt.hcl ../
     - terragrunt apply -auto-approve
  when: manual
  only: 
    refs:
      - dev
    variables:
      - ($PIPELINE ==  "default" || $PIPELINE == $DIR_NAME)

# STAGES

stages:
  - plan
  - deploy

# PLAN

# Executing plan-all if PIPELINE="all"
plan_all:
  stage: plan
  script:
    - terragrunt plan-all 
  only:
    variables:
      - $PIPELINE =~ /all/

# Plan only for particular folder. If it was changed. Or variable PIPELINE was set to %%FOLDERNAME%%. Or default run
plan_moduletest_production:
  stage: plan
  variables:
    DIR_NAME: "moduletest"
  extends: .plan_part_production
  only:
    changes:
      - moduletest/**/*
    variables:
      - $PIPELINE =~ /all|default|moduletest/

plan_moduletest_stage:
  stage: plan
  variables:
    DIR_NAME: "moduletest"
  extends: .plan_part_stage
  only:
    changes:
      - moduletest/**/*
    variables:
      - $PIPELINE =~ /all|default|moduletest/

plan_moduletest_dev:
  stage: plan
  variables:
    DIR_NAME: "moduletest"
  extends: .plan_part_dev
  only:
    changes:
      - moduletest/**/*
    variables:
      - $PIPELINE =~ /all|default|moduletest/

plan_virtual-machine_dev:
  stage: plan
  variables:
    DIR_NAME: "virtual-machine"
  extends: .plan_part_dev
  only:
    changes:
      - virtual-machine/**/*
    variables:
      - $PIPELINE =~ /all|default|virtual-machine/

plan_virtual-machine_stage:
  stage: plan
  variables:
    DIR_NAME: "virtual-machine"
  extends: .plan_part_stage
  only:
    changes:
      - virtual-machine/**/*
    variables:
      - $PIPELINE =~ /all|default|virtual-machine/

plan_bucket-copy-function_dev:
  stage: plan
  variables:
    DIR_NAME: "bucket-copy-function"
  extends: .plan_part_dev
  only:
    changes:
      - bucket-copy-function/**/*
    variables:
      - $PIPELINE =~ /all|default|bucket-copy-function/


# DEPLOY

# Executing apply-all if PIPELINE="all"
apply_all:
  stage: deploy
  script:
    - terragrunt apply-all --terragrunt-non-interactive
  dependencies:
    - plan_all
  when: manual
  only:
    refs:
      - master
    variables:
      - $PIPELINE =~ /all/

# Apply only for particular folder. If it was changed. Or variable PIPELINE was set to %%FOLDERNAME%%. Or default run
apply_moduletest_production:
  stage: deploy
  variables:
    DIR_NAME: "moduletest"
  extends: .apply_part_production
  needs:
    - job: plan_moduletest_production
      artifacts: true
  only: 
    changes:
      - moduletest/**/*

apply_moduletest_stage:
  stage: deploy
  variables:
    DIR_NAME: "moduletest"
  extends: .apply_part_stage
  needs:
    - job: plan_moduletest_stage
      artifacts: true
  only: 
    changes:
      - moduletest/**/*

apply_moduletest_dev:
  stage: deploy
  variables:
    DIR_NAME: "moduletest"
  extends: .apply_part_dev
  needs:
    - job: plan_moduletest_dev
      artifacts: true
  only: 
    changes:
      - moduletest/**/*

apply_virtual-machine_dev:
  stage: deploy
  variables:
    DIR_NAME: "virtual-machine"
  extends: .apply_part_dev
  needs:
    - job: plan_virtual-machine_dev
      artifacts: true
  only: 
    changes:
      - virtual-machine/**/*

apply_virtual-machine_stage:
  stage: deploy
  variables:
    DIR_NAME: "virtual-machine"
  extends: .apply_part_stage
  needs:
    - job: plan_virtual-machine_stage
      artifacts: true
  only: 
    changes:
      - virtual-machine/**/*

apply_bucket-copy-function_dev:
  stage: deploy
  variables:
    DIR_NAME: "bucket-copy-function"
  extends: .apply_part_dev
  needs:
    - job: plan_bucket-copy-function_dev
      artifacts: true
  only: 
    changes:
      - bucket-copy-function/**/*



