instance_name        = "gce-pwk-sandbox--database-dev-1"              
instance_machinetype = "custom-2-4096"                          
disk_sizes           = ["100"]                             
disks_type           = "pd-standard"                        
boot_disk_size       = "20"                                 
image                = "centos-7-v20190916"        
labels               = { "cron_schedule" = "no" }             
region               = "europe-west4"                           
region_zone          = "europe-west4-a"                            
project_name         = "pwk-sandbox-20211208-xma1xx" 
network              = "pw-vpc-01"
subnetwork           = "pw-subnet-01"
preemtible           = false                                    
network-tags         = ["pwk-database"]                   
 


