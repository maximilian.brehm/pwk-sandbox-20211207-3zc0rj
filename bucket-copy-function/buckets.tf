resource "google_storage_bucket" "cloud-function-bucket" {
  name     = var.cf_bucket_name
  project  = var.project_name
  location = var.region

  #uniform_bucket_level_access = true
  storage_class = "NEARLINE"
  force_destroy = false

  versioning {
    enabled = false
  }
}


resource "google_storage_bucket" "master-bucket" {
  name     = var.bucket_name
  project  = var.project_name
  location = var.region

  #uniform_bucket_level_access = true
  storage_class = "NEARLINE"
  force_destroy = false

  versioning {
    enabled = false
  }
}

resource "google_storage_bucket" "slave-bucket-1" {
  name     = var.bucket_name_slave_1
  project  = var.project_name
  location = var.region

  #uniform_bucket_level_access = true
  storage_class = "NEARLINE"
  force_destroy = false

  versioning {
    enabled = false
  }
}
