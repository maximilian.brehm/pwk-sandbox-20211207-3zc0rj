module "bucket_copy" {
  source = "git::git@gitlab.com:peakwork/it/lad/terraform-infra/modules/modules_pwk-set/bucket_sync_cf_code.git?ref=1.0.0-dev"
                          
  project_name         = var.project_name 
  bucket_name          = var.bucket_name     
  cf_bucket_name       = var.cf_bucket_name

 # depends_on   = [ 
 #       google_storage_bucket.cloud-function-bucket.name,
 #       google_storage_bucket.master-bucket.name,
 #       google_storage_bucket.slave-bucket-1.name
 # ]                                                                     
}
