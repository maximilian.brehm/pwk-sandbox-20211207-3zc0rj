instance_name        = "gce-pwk-sandbox-stage-1"              
instance_machinetype = "custom-2-2048"                          
disk_sizes           = ["60"]                             
disks_type           = "pd-standard"                        
boot_disk_size       = "20"                                 
image                = "centos-7-v20190916"        
labels               = { "cron_schedule" = "yes" }             
region               = "europe-west4"                           
region_zone          = "europe-west4-a"                            
project_name         = "pwk-sandbox-20220304-i7n0tc" 
network              = "pw-vpc-01"
subnetwork           = "pw-subnet-01"
preemtible           = false                                    
network-tags         = ["pwk-webservice"]                   
external_ip          = ["yes"]  

