variable "instance_name" {
}
variable "instance_machinetype" {
}
variable "disk_sizes" {
}
variable "disks_type" {
}
variable "boot_disk_size" {
}
variable "image" {
}
variable "labels" {
}
variable "region" {
    default = "europe-west4"
}
variable "region_zone" {
    default = "europe-west4-a"
}
variable "project_name" {
}
variable "network" {
}
variable "subnetwork" {
}
variable "preemtible" {
}
variable "network-tags" {
}
variable "external_ip" {
    default = []
}
