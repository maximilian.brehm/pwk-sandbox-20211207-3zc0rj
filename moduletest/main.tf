module "webservice_test" {
  source = "git::git@gitlab.com:peakwork/it/cad/infrastructure/modules_cust/test_module_vm_sa_test.git"
  #source = "git::https://gitlab.com/peakwork/it/cad/infrastructure/modules_cust/test_module_vm_sa_test.git"

  instance_name        = var.instance_name            
  instance_machinetype = var.instance_machinetype                
  disk_sizes           = var.disk_sizes                            
  boot_disk_size       = var.boot_disk_size                                  
  image                = var.image     
  labels               = var.labels                                       
  region               = var.region                           
  region_zone          = var.region_zone
  service_account      = ""                                       
  project_name         = var.project_name 
  network              = var.network
  subnetwork           = var.subnetwork
  network-tags         = var.network-tags                      
  external_ip          = var.external_ip                                                                                        
}
